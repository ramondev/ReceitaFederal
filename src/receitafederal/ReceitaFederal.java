
package receitafederal;

import bibliotecas.CPF;
import javax.swing.JOptionPane;


public class ReceitaFederal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        openMenu();
        
    }
    
    public static void openMenu(){
        String menu = "RECEITA FEDERAL\n\n"
                + "Escolha uma das opções abaixo:\n"
                + "1 - Formatar um CPF\n"
                + "2 - Limpar um CPF\n"
                + "3 - Validar um CPF\n"
                + "4 - Gerar um novo CPF\n"
                + "0 - Sair\n";
        
        String op = JOptionPane.showInputDialog(menu);
        
        switch (op){
            case "1":
                String fcpf = JOptionPane.showInputDialog("Digite o seu CPF:");
                JOptionPane.showMessageDialog(null, CPF.format(fcpf));
                openMenu();
                break;
                
            case "2":
                String lcpf = JOptionPane.showInputDialog("Digiteo seu CPF:");
                JOptionPane.showMessageDialog(null, CPF.clear(lcpf));
                openMenu();
                break;
            
            case "3":
                String vcpf = JOptionPane.showInputDialog("Digiteo seu CPF:");
                if(CPF.validate(vcpf)) {
                    JOptionPane.showMessageDialog(null, "CPF válido!");
                } else {
                    JOptionPane.showMessageDialog(null, "CPF inválido!");
                }
                openMenu();
                break;
                
            case "4":
                JOptionPane.showMessageDialog(null, CPF.generate());
                openMenu();
                break;

            case "0":
                JOptionPane.showMessageDialog(null, "Obrigado pela preferência!");
                break;
                
            default:
                JOptionPane.showMessageDialog(null, "Opção inválida");
                openMenu();
                break;
        }
    }
    
}
