package bibliotecas;

public class CPF {

    public static String clear(String cpf) {

        cpf = cpf.replace("-", "").replace(".", "").replace(" ", "");
        return cpf;
    }

    public static String format(String cpf) {

        String[] div = cpf.split("");

        return div[0] + div[1] + div[2] + "." + div[3] + div[4] + div[5] + "."
                + div[6] + div[7] + div[8] + "-" + div[9] + div[10];
    }

    public static boolean validate(String cpf) {

        cpf = clear(cpf);

        int[] CPF = new int[11];

        for (int i = 0; i <= 10; i++) {

            CPF[i] = Integer.parseInt(String.valueOf(cpf.charAt(i)));

        }

        int primeiraSoma = 0;
        int segundaSoma = 0;

        for (int i = 0; i < 9; i++) {
            primeiraSoma = primeiraSoma + (CPF[i] * (10 - i));
        }

        if (11 - (primeiraSoma % 11) == CPF[9] && 11 - (primeiraSoma % 11) > 2
                || CPF[9] == (primeiraSoma % 11) && (primeiraSoma % 11) < 2) {

            for (int i = 0; i < 10; i++) {
                segundaSoma = segundaSoma + (CPF[i] * (11 - i));
            }

        } else {
            return false;
        }

        if (11 - (segundaSoma % 11) == CPF[10] && 11 - (segundaSoma % 11) > 2
                || CPF[10] == (segundaSoma % 11) && (segundaSoma % 11) < 2) {
            return true;
        } else {
            return false;
        }
    }

    public static String generate() {

        int[] ncpf = new int[11];

        for (int i = 0; i < 9; i++) {
            ncpf[i] = (int) (Math.random() * 10);
        }

        // GERANDO O PRIMEIRO DIGITO VERIFICADOR
        int somador = 0, mult = 10;

        for (int i = 0; i < 9; i++) {
            somador += ncpf[i] * mult;
            mult--;
        }

        if (somador % 11 < 2) {
            ncpf[9] = 0;
        } else {
            ncpf[9] = 11 - (somador % 11);
        }

        // GERANDO O SEGUNDO DIGITO VERIFICADOR
        somador = 0;
        mult = 11;

        for (int i = 0; i < 10; i++) {
            somador += ncpf[i] * mult;
            mult--;
        }

        if (somador % 11 < 2) {
            
            ncpf[10] = 0;
            
        } else {
            
            ncpf[10] = 11 - (somador % 11);
            
        }
        
        // TRANSFORMANDO ARRAY INTEIROS EM UMA STRING
        
        String cpf = "";
        
        for (int i = 0; i < 11; i++) {
            cpf += ncpf[i];
        }

        return format(cpf);

    }
}
